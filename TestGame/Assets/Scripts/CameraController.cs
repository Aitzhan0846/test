﻿using UnityEngine;
using Cinemachine;


public class CameraController : MonoBehaviour
{
    [SerializeField]
    private float lookSpeed = 1;

    #region PrivateVars
    private CinemachineFreeLook cinemachine;
    private CameraLook camLook;
    #endregion

    /// <summary>
    /// Instantiate input system and get cinemachine component
    /// </summary>
    private void Awake()
    {
        camLook = new CameraLook();
        cinemachine = GetComponent<CinemachineFreeLook>();
    }

    /// <summary>
    /// Enable input
    /// </summary>
    private void OnEnable()
    {
        camLook.Enable();
    }

    /// <summary>
    /// Disable input
    /// </summary>
    private void OnDisable()
    {
        camLook.Disable();
    }

    /// <summary>
    /// Runs on every frame
    /// Supply x and y axis values to cinemachine free look camera through input
    /// </summary>
    void Update()
    {
        Vector2 delta = camLook.Cam.Look.ReadValue<Vector2>();
        cinemachine.m_XAxis.Value += delta.x * 200 * lookSpeed * Time.deltaTime;
        cinemachine.m_YAxis.Value += delta.y * lookSpeed * Time.deltaTime;
    }
}
