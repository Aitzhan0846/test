﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CurrentLevel
{
    Level_1,
    Level_2
}
public class UIManager : MonoBehaviour
{
    public CurrentLevel curLevel;

    public Text txtCurLevel;
    public Text txtNextLevel;

    // Start is called before the first frame update
    void Start()
    {
        txtCurLevel.text = $"Current Level: {CurrentLevel.Level_1.ToString()}";
        txtNextLevel.text = $"Next Level: {CurrentLevel.Level_2.ToString()}";
    }

   public void NextLevel()
    {
        curLevel = CurrentLevel.Level_2;
    }
}
