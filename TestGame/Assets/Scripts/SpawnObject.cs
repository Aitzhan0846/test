﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObject : MonoBehaviour
{
    [SerializeField] private List<GameObject> prefabs;
    // Start is called before the first frame update
    void Awake()
    {
        Spawn();
    }

    void Spawn()
    {
        float randX;
        float randZ;
        for (int i = 0; i < prefabs.Count; i++)
        {
            randX = Random.Range(-3.3f, 3.3f);
            randZ = Random.Range(-5.5f, -7f);

            GameObject npc=Instantiate(prefabs[i], new Vector3(randX, 0, randZ), Quaternion.identity);

            NPController.npc.Add(npc);
            
        }
    }
}
