﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskCheck : MonoBehaviour
{
    public Slider progressBar;
    public int task_1;
   
    public void Check()
    {
        Invoke("Winner", 3);
    }
    void Winner()
    {
        progressBar.value = task_1;
        if (task_1 == 3)
        {
            print("Win");
            gameObject.GetComponent<UIManager>().NextLevel();
        }
          
    }
}
