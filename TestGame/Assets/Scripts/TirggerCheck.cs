﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ColorPlane
{
    Red,
    Blue,
    Green
}
public class TirggerCheck : MonoBehaviour
{
    public ColorPlane colorPlane;
    private TaskCheck taskCheck;
    private void Start()
    {
        taskCheck = FindObjectOfType<TaskCheck>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == colorPlane.ToString())
        {
            other.transform.GetComponent<NPCMovement>().enabled = false;
            taskCheck.task_1 += 1;
            taskCheck.Check();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == colorPlane.ToString())
        {
            other.transform.GetComponent<NPCMovement>().enabled = true;
            taskCheck.task_1 -= 1;
            taskCheck.Check();
        }

    }
}
