// GENERATED AUTOMATICALLY FROM 'Assets/Input/CameraLook.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @CameraLook : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @CameraLook()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""CameraLook"",
    ""maps"": [
        {
            ""name"": ""Cam"",
            ""id"": ""81239825-5187-4012-90a4-3efe7e95078e"",
            ""actions"": [
                {
                    ""name"": ""Look"",
                    ""type"": ""Value"",
                    ""id"": ""4e88bd38-e91d-47e1-b00e-707a16c5670b"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""95d25f2f-5bb2-4289-8b4f-d783cbcf7e51"",
                    ""path"": ""<Gamepad>/rightStick"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Look"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Cam
        m_Cam = asset.FindActionMap("Cam", throwIfNotFound: true);
        m_Cam_Look = m_Cam.FindAction("Look", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Cam
    private readonly InputActionMap m_Cam;
    private ICamActions m_CamActionsCallbackInterface;
    private readonly InputAction m_Cam_Look;
    public struct CamActions
    {
        private @CameraLook m_Wrapper;
        public CamActions(@CameraLook wrapper) { m_Wrapper = wrapper; }
        public InputAction @Look => m_Wrapper.m_Cam_Look;
        public InputActionMap Get() { return m_Wrapper.m_Cam; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(CamActions set) { return set.Get(); }
        public void SetCallbacks(ICamActions instance)
        {
            if (m_Wrapper.m_CamActionsCallbackInterface != null)
            {
                @Look.started -= m_Wrapper.m_CamActionsCallbackInterface.OnLook;
                @Look.performed -= m_Wrapper.m_CamActionsCallbackInterface.OnLook;
                @Look.canceled -= m_Wrapper.m_CamActionsCallbackInterface.OnLook;
            }
            m_Wrapper.m_CamActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Look.started += instance.OnLook;
                @Look.performed += instance.OnLook;
                @Look.canceled += instance.OnLook;
            }
        }
    }
    public CamActions @Cam => new CamActions(this);
    public interface ICamActions
    {
        void OnLook(InputAction.CallbackContext context);
    }
}
